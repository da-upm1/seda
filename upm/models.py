from django.db import models


class School(models.Model):
    name = models.CharField(max_length=100, default="UPM")
    code = models.CharField(max_length=100, default="UPM")
    shortcode = models.CharField(max_length=128, blank=True, null=True)


    def __str__(self):
        return self.name


class Plan(models.Model):
    name = models.CharField(max_length= 100, default="Grado en Cosas")
    code = models.CharField(max_length=100, default="10IC")

    def __str__(self):
        return self.name


class Department(models.Model):
    name = models.CharField(max_length=100, default='')
    code = models.CharField(max_length= 100, default='')

    def __str__(self):
        return self.name


class Grade(models.Model):
    code = models.CharField(max_length=10, default='')
    name = models.CharField(max_length=100, default='')

    # Relations between fields
    school = models.ForeignKey('School', on_delete=models.SET_NULL, null=True)


    def __str__(self):
        return self.name


class Professor(models.Model):
    name = models.CharField(max_length=100, default="")
    code_id = models.CharField(max_length=100, default="")
    email = models.EmailField(max_length=124, default="mail@upm.es")
    observations = models.TextField(max_length=248, null=True, blank=True)
    position = models.CharField(max_length=64,default="")


    def __str__(self):
        return self.name


class Subject(models.Model):
    code = models.CharField(max_length=10)
    name = models.CharField(max_length=100)
    grade = models.ForeignKey('Grade', on_delete=models.CASCADE, default=1)
    professor = models.ManyToManyField('Professor')

    # Relations between fields
    department = models.ForeignKey(to = 'Department', on_delete=models.CASCADE)
    grade = models.ManyToManyField(to='Grade')
    def __str__(self):
        return self.name



