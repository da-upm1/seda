from rest_framework.generics import ListAPIView
from upm import serializers, functions
from upm.models import School, Grade, Subject, Professor


class SchoolEndpoint(ListAPIView):
    #functions.functions().lavadora()
    serializer_class = serializers.SchoolSerializer

    queryset = School.objects.all()


class GradeEndpoint(ListAPIView):

    serializer_class = serializers.GradeSerializer

    def get_queryset(self):
        return Grade.objects.all().filter(
            school__code=self.kwargs['SchoolCode']
        )


class SubjectEndpoint(ListAPIView):

    serializer_class = serializers.SubjectSerializer

    def get_queryset(self):
        return Subject.objects.all().filter(
            grade__id=self.kwargs["GradeID"]
        )


class ProfessorEndpoint(ListAPIView):

    serializer_class = serializers.ProfessorSerializer

    def get_queryset(self):
        return Professor.objects.all().filter(subject__id=self.kwargs["SubjectID"])

