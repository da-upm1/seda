from django.contrib import admin
from upm.models import School, Subject, Department, Grade, Professor

admin.site.register(School)
admin.site.register(Subject)
admin.site.register(Department)
admin.site.register(Grade)
admin.site.register(Professor)

#TODO Add action to update external content from api use functions.py

