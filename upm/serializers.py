from rest_framework.serializers import ModelSerializer, SerializerMethodField

from upm.models import School, Grade, Subject, Professor, Department

class SchoolSerializer(ModelSerializer):

    class Meta:
        model = School
        fields = "__all__"


class DepartmentSerializer(ModelSerializer):

    class Meta:
        model = Department
        fields = "__all__"


class GradeSerializer(ModelSerializer):

    class Meta:
        model = Grade
        fields = "__all__"


class SubjectSerializer(ModelSerializer):
    professor = SerializerMethodField()

    def get_professor(self, subject):
        return ProfessorSerializer(subject.professor, many=True, context=self.context).data

    class Meta:
        model = Subject
        fields = "__all__"


class ProfessorSerializer(ModelSerializer):

    class Meta:
        model = Professor
        fields = "__all__"