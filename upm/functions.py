import json

import requests as req

from upm.models import School, Subject, Department


class functions():
    def __init__(self):
        self.urls = {
        "centros"       :   "https://www.upm.es/wapi_upm/academico/comun/index.upm/v2/centro.json",
        "dptos"         :   "https://www.upm.es/wapi_upm/academico/comun/index.upm/v2/departamento.json",
        "planes"        :   "https://www.upm.es/wapi_upm/academico/comun/index.upm/v2/planes.json",
        "asignaturas"   :   "https://www.upm.es/wapi_upm/academico/comun/index.upm/v2/asignaturas.json"
        }
    
    # CENTROS:

    def __get(self, url):
        response = req.get(url)
        return response

    def getCentros(self):
        return self.__get(self.urls["centros"])
    
    def getCentro(self, codCentro):
        return self.__get(self.urls["centros"]+"/{}".format(codCentro))

    def getPlanesCentro(self, codCentro):
        return self.__get(self.urls["centros"]+"/{}/planes".format(codCentro))

    # PLANES:
    def getPlanes(self):
        return self.__get(self.urls["planes"])

    def getPlan(self, codPlan):
        return self.__get(self.urls["planes"]+"/{}".format(codPlan))

    def getAsignaturasPlan(self, codPlan):
        return self.__get(self.urls["planes"]+"/{}/asignaturas".format(codPlan))
    
    # ASIGNATURAS:
    def getAsignatura(self, codAsignatura, codPlan):
        return self.__get(self.urls["asignaturas"]+"/{}/{}".format(codAsignatura, codPlan))

    def getGruposAsignatura(self, codAsignatura, codPlan):
        return self.__get(self.urls["asignaturas"]+"/{}/{}/grupos_matricula".format(codAsignatura, codPlan))

    # DEPARTAMENTOS:
    def getAsignaturasDptoPlan(self, codDpto, codPlan):
        return self.__get(self.urls["dptos"]+"/{}/{}/asignaturas".format(codDpto, codPlan))
    
    def getDepartamentosCentro(self, codCentro, year):
        return self.__get(self.urls["centros"]+"/{}/departamentos?anio={}".format(codCentro, year))

    def getDepartamentos(self):
        return self.__get(self.urls["dptos"])

    def getDepartamento(self, codDpto):
        return self.__get(self.urls["dptos"]+"/{}".format(codDpto))


    def lavadora(self):
        goteras = functions().getCentros().content
        for index, gotera in enumerate(json.loads(goteras)):
            print("gotera" + str(gotera))
            School.objects.create(
                name=gotera["nombre"],
                id=gotera["codigo"]
    
            )

    def createAsignatura(self):
        subjects = functions().getAsignatura().content
        for index,subject in enumerate(json.loads(subjects)):
            print("asignatura" + str(subject))
            Subject.objects.create(
                name=subjects["nombre"],
                id=subjects["codigo"]

            )


    def createDepartment(self):
        departments = functions().getDepartamentos().content
        for index,department in enumerate(json.loads(departments)):
            print("Departamento" + str(department))
            Department.objects.create(
                name=departments["nombre"],
                id=departments["codigo"]
            )


