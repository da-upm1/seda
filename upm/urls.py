from django.urls import path, re_path
from upm import views

urlpatterns = [path("schools/", views.SchoolEndpoint.as_view(), name = "schools"),
               re_path(r'school-grades/(?P<SchoolID>[0-9]+)/$', views.GradeEndpoint.as_view(), name = "grades"),
                   re_path(r"subjects/by_grade_id/(?P<GradeID>[0-9]+)/$", views.SubjectEndpoint.as_view(), name = "subjects"),
               re_path(r"professors/by_school_id/(?P<SubjectID>[0-9]+)/$", views.ProfessorEndpoint.as_view(), name = "professors")
               ]