# Generated by Django 3.0.3 on 2020-03-31 20:23

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('upm', '0004_auto_20200328_1141'),
    ]

    operations = [
        migrations.AddField(
            model_name='subject',
            name='grade',
            field=models.ForeignKey(default=0, on_delete=django.db.models.deletion.CASCADE, to='upm.Grade'),
        ),
    ]
