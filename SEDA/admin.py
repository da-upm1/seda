from django.contrib import admin

from SEDA.models import UserManagePermissions

#Customisation
admin.site.site_header = "SEDA-UPM"
admin.site.site_title = "SEDA-UPM"

admin.site.register(UserManagePermissions)
