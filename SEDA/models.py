from django.contrib.auth.models import User, AbstractUser
from django.db import models


class UserManagePermissions(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    # schools that the user can manaage
    schools = models.ManyToManyField('upm.School')

    class Meta:
        permissions = (
            ("evalua_school_admin", "Can manage evaluas"),
        )

    def __str__(self):
        return self.user.username

