# Generated by Django 3.0.4 on 2020-03-13 16:45

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Student',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='', max_length=128)),
                ('reg_number', models.CharField(default='', max_length=128)),
                ('email', models.EmailField(default='', max_length=128)),
                ('phone', models.IntegerField()),
                ('dni', models.CharField(default='', max_length=32)),
            ],
        ),
    ]
