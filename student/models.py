from django.db import models

class Student(models.Model):

    name = models.CharField(max_length=128)
    #code_id = models.CharField(max_length=128, )
    reg_number = models.CharField(max_length = 128, default="") # Matricula del estudiante
    email = models.EmailField(max_length=128, default="mail@alumnos.upm.es")
    phone = models.IntegerField(default=0)
    dni = models.CharField(max_length=32, default="53454657L")


    def __str__(self):
        return self.name