from django.shortcuts import render
from rest_framework.generics import ListAPIView
from student import serializers
from student.models import Student
from rest_framework.permissions import IsAdminUser


class StudentEndpoint(ListAPIView):
    permission_classes = [IsAdminUser]
    serializer_class = serializers.StudentSerializer

    queryset = Student.objects.all()
