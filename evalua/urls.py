
from django.contrib import admin
from django.urls import path, include
from rest_framework_swagger.views import get_swagger_view

from evalua.views import CreateEvalua

urlpatterns = [
    path('new/', view=CreateEvalua.as_view()),
            ]