# Generated by Django 3.0.3 on 2020-03-31 20:27

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('upm', '0006_auto_20200331_2024'),
        ('evalua', '0005_auto_20200331_2023'),
    ]

    operations = [
        migrations.AlterField(
            model_name='evalua',
            name='professor',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='upm.Professor'),
        ),
        migrations.AlterField(
            model_name='evalua',
            name='subject',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='upm.Subject'),
        ),
    ]
