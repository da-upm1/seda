# Generated by Django 3.0.3 on 2020-03-30 23:21

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('upm', '0004_auto_20200328_1141'),
        ('evalua', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='evalua',
            name='professor',
            field=models.ForeignKey(default=0, on_delete=django.db.models.deletion.CASCADE, to='upm.Professor'),
        ),
        migrations.AddField(
            model_name='evalua',
            name='subject',
            field=models.ForeignKey(default=0, on_delete=django.db.models.deletion.CASCADE, to='upm.Subject'),
        ),
    ]
