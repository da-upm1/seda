from rest_framework.serializers import ModelSerializer

from evalua.models import Evalua


class EvaluaSerializer(ModelSerializer):

    class Meta():
        model = Evalua
        fields = ('professor', 'subject', 'category', 'reason')
