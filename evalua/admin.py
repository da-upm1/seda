from django.contrib import admin
from evalua.models import Evalua


# Register your models here.


class EvaluaAdmin(admin.ModelAdmin):
    list_filter = ('timestamp', 'professor', 'subject', 'category', 'status', 'confirmed')
    list_display = ('id', 'professor', 'subject', 'category', 'status', 'confirmed')
    list_editable = ('status',)
    # exclude = ('')
    readonly_fields = ('professor', 'subject', 'category', 'reason')

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(school=request.user)


admin.site.register(Evalua, EvaluaAdmin)
