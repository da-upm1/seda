from django.shortcuts import render
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import AllowAny

from evalua.models import Evalua
from evalua.serializers import EvaluaSerializer

""# Create your views here.


class CreateEvalua(CreateAPIView):
    queryset = Evalua.objects.all()
    serializer_class = EvaluaSerializer
    permission_classes = [AllowAny]
