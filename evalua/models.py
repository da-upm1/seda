import random
import string

from django.db import models
from upm.models import Professor, Subject
from student.models import Student

# Create your models here.

CHARACTERS = (
    string.ascii_letters
    + string.digits
    + '-._~'
)


def generate_unique_key():
    return ''.join(random.sample(CHARACTERS, 15))


class Evalua(models.Model):
    CATEGORIES = (
        ('0', 'Complaint'),
        ('1', 'Congratulation'),
    )

    STATUS = (
        ('0', 'Unseen'),
        ('1', 'Seen'),
    )
    id = models.CharField(max_length=6, primary_key=True, editable=False)
    reason = models.TextField()
    category = models.CharField(choices=CATEGORIES, max_length=15, default=CATEGORIES[0])
    status = models.CharField(choices=STATUS, max_length=15)
    confirmed = models.BooleanField()
    timestamp = models.DateTimeField(auto_now_add=True)
    professor = models.ForeignKey('upm.Professor', on_delete=models.CASCADE, default=1)
    subject = models.ForeignKey('upm.Subject', on_delete=models.CASCADE, default=1)
    group = models.ForeignKey('EvaluaGroup', on_delete=models.SET_NULL, null= True, blank= True)

    def __str__(self):
        return self.id

    @classmethod
    def create(cls, id):
        randomid = generate_unique_key()
        # Ensure random key is not in db
        while Evalua.objects.filter(id=randomid).count() != 0:
            randomid = generate_unique_key()
        evalua = cls(id=id)
        evalua.id = randomid
        # TODO send email with ID to student
        return evalua

    def save(self, *args, **kwargs):
        # TODO send email to student with info about status change
        pass



class EvaluaGroup(models.Model):

    name = models.CharField(max_length=32)
    description = models.TextField(max_length=256) #Should it be more?


